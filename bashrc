
# LICENSE: GNU AGPL Version 3
#
# Simple bash code for showing the current git branch, uncommited
# changes and notice of all items pushed.
#
# Copyright (C) 2014  Ron Bravo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# 	<http://www.gnu.org/licenses/>.
#
# References:
#	- http://askubuntu.com/questions/249174/prepend-current-git-branch-in-terminal
#	- http://bytebaker.com/2012/01/09/show-git-information-in-your-prompt/
#	- http://misc.flogisoft.com/bash/tip_colors_and_formatting
#

function git-dirty() {
	# Grab the last line from the git status message.
	status=$(git status 2>/dev/null | tail -n 1)
	nocommits="nothing to commit"

	# Check if status says there is no change.
	#if [[ $status != "nothing to commit, working directory clean" ]] || [[ $status != "nothing to commit (working directory clean)" ]];
	if [[ $status == *"nothing to commit"* ]]
	then
		echo ""
	else
		echo "*"
	fi
}

function git-info() {
	# Make sure that we are inside of a git project directory
	# by looking for the hidden ".git" directory.
	if [ -d .git ]
	then
		# Get the current git branch.
		status=$(git symbolic-ref HEAD 2>/dev/null | awk -F/ {'print $NF'})

		# Make sure the status has a value.
		if [[ $status == "" ]]
		then
			echo ""
		else
			echo " "\($status\)$(git-dirty)
		fi
	else
		echo ""
	fi;
}

# Edit the prompt to color the git branch and status.
PS1="${PS1:0:-3}\e[33m\$(git-info)\e[0m "
